var canvas = document.getElementById('tutorial');
if (canvas.getContext) {
    var ctx = canvas.getContext('2d');
    // code de dessin dans le canvas
}

let caseWidth = 0; //largeur de case, sera définie en fonctio du nombre de cases
let caseHeight = 0; // hauteur de case
let fontHeight = 0;// hauteur de police
let colNumber = 0; // nombre de colonnes
let rowNumber = 0;// nombre de lignes

const caseMargin = 2;
const gameWidth = 900; //largeur du jeu
const gameHeight = 500; //hauteur du jeu

const tableauSolution = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0],
    [0, 0, 0, 1, 1, 0],
    [0, 0, 0, 1, 1, 0],
    [0, 0, 1, 1, 1, 0],
    [0, 0, 0, 1, 1, 0]
];// résultat titanic
let tableauVerif = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
];// tableau permettant la vérif du titanic

const iSuperman = 3; //nombre de colonnes des indices de superman
const jSuperman = 4;// nombre de lignes des indices de superman
const iTitanic = 1;
const jTitanic = 1;

const sideTitanic = [1, 2, 2, 3, 2] //chiffres sur le coté du tableau
const topTitanic = [0, 1, 5, 4, 0] // chiffres au dessus du tableau
const sideSuperman = [
    [8],
    [3, 3],
    [2, 4, 2],
    [2, 7],
    [3, 4],
    [5, 2],
    [2, 2, 2],
    [2, 7],
    [3, 4],
    [5, 2],
    [2, 2, 2],
    [2, 2],
    [6],
    [4]
];// tableau des indices de superman qui sont sur le coté

const topSuperman = [
    [4],
    [7],
    [2, 5],
    [1, 2, 1, 3],
    [1, 2, 2, 2],
    [1, 2, 2, 2],
    [1, 3, 3],
    [2, 6],
    [7],
    [4]
];// tableau des indices de <superman qui son tsur le haut 

const sideOctopus = [
[9],
[11],
[13],
[4, 5],
[3, 4],
[3, 4, 3],
[3, 4, 6],
[8, 4, 8],
[6, 5, 10],
[2, 5, 4, 3],
[9, 5, 3, 3],
[11, 5, 3, 4],
[12, 5, 4, 4],
[18, 2, 5, 3],
[4, 16, 12, 1],
[3, 17, 12],
[4, 18, 3, 5],
[3, 19, 4, 2],
[6, 10, 9, 1],
[7, 10, 7, 2],
[2, 9, 4, 2],
[14, 2],
[3, 17, 3],
[4, 25, 3],
[8, 27, 5],
[45],
[27, 12],
[25, 6],
[26],
[26],
[27],
[34],
[36],
[8, 27],
[6, 27],
[2, 12, 9],
[12, 6],
[14, 5],
[15, 5],
[15, 4],
[3, 4, 11, 4],
[7, 4, 10, 4, 3],
[3, 10, 5, 5, 3, 4],
[2, 8, 5, 4, 3, 5],
[1, 6, 4, 4, 1, 3, 2],
[5, 6, 3, 2],
[5, 7, 4, 3],
[13, 8, 8],
[13, 5, 3, 5],
[13, 3, 3],
[13, 2, 3],
[3, 2, 3],
[3, 3, 3],
[3, 4, 5],
[3, 2, 9],
[4, 2, 6],
[8, 2],
[4]
];

const topOctopus = [
    [1],
    [1, 2],
    [2, 7],
    [3, 10],
    [3, 11, 4],
    [2, 12, 3],
    [4, 13, 3],
    [5, 13, 2],
    [5, 12, 3],
    [6, 11, 3, 4],
    [5, 10, 4, 6],
    [4, 8, 3, 8],
    [4, 11, 3, 4, 3],
    [4, 12, 3, 4, 2],
    [4, 13, 4, 4, 3],
    [5, 4, 13, 5, 4, 2],
    [7, 5, 13, 7, 4, 2],
    [8, 7, 20, 4, 2],
    [4, 34, 4, 3],
    [3, 3, 29, 4, 3],
    [3, 2, 28, 5],
    [4, 2, 28, 6],
    [4, 1, 38],
    [4, 39],
    [5, 40],
    [46, 7],
    [13, 30, 7],
    [11, 6, 6, 12, 3, 4],
    [8, 5, 6, 13, 2, 3],
    [5, 5, 4, 7, 3, 3],
    [4, 5, 4, 7, 2, 4],
    [4, 5, 4, 8, 3],
    [4, 4, 5, 14],
    [2, 4, 4, 5, 11],
    [7, 4, 4, 5, 9],
    [8, 4, 4, 6, 2],
    [5, 4, 4, 4, 7, 2],
    [4, 3, 3, 3, 10],
    [4, 3, 4, 3, 14],
    [4, 6, 3, 14],
    [4, 6, 3, 14],
    [12, 3, 3],
    [9, 3, 3, 2],
    [9, 3, 3, 2],
    [2, 3, 4, 3, 3],
    [3, 3, 6],
    [2, 4, 5],
    [3, 4],
    [1, 8],
    [4]
  ];

  iOctopus = 7;
  jOctopus = 6;

/*let sideSupermanHeight = 0;
function calculHauteur (tableau) {
for (i=0; i<tableau.length; i++){
    let j = tableau[i].length;
    console.log(sideSupermanHeight);
   if (j<sideSupermanHeight){
       sideSupermanHeight=j;
       console.log(sideSupermanHeight);
   }
}
return sideSupermanHeight;
}
calculHauteur (sideSuperman);
console.log(sideSupermanHeight);

/*for (i = 0; i < 4; i++) {
    let j = 0;
    while (sideSuperman[i][j] !== undefined || j < 4) {
        j++;
    }
    sideSupermanHeight = j;
    console.log(sideSupermanHeight);
}*/


/*dessine le canvas */
function grille(iG, jG, rowN, colN, caseW, caseH) {

    for (var i = iG; i < colN; i++) {
        for (var j = jG; j < rowN; j++) {
            ctx.fillRect((i * (caseW + caseMargin)), (j * (caseH + caseMargin)), caseW, caseH);

        }
    }
    for (var i = 0; i < iG; i++) {
        for (var j = 0; j < rowN; j++) {
            ctx.fillStyle = 'cyan';
            ctx.fillRect((i * (caseW + caseMargin)), (j * (caseH + caseMargin)), caseW, caseH);
        }
    }
    for (var i = 0; i < colN; i++) {
        for (var j = 0; j < jG; j++) {
            ctx.fillStyle = 'cyan';
            ctx.fillRect((i * (caseW + caseMargin)), (j * (caseH + caseMargin)), caseW, caseH);
        }
    }
}
//nettoie la grille
function clearGrille() {

    ctx.clearRect(0, 0, 700, 700);

}
// dessine les indices pour titanic
function drawTitanic(fontH, caseH, caseW) {
    ctx.font = fontH + 'px serif';
    for (var i = 0; i < sideTitanic.length; i++) {
        ctx.fillStyle = 'black';
        ctx.fillText(sideTitanic[i], 20, i * (caseH + caseMargin) + 1.8 * caseH);
        ctx.fillText(topTitanic[i], 1.2 * (caseW + caseMargin) + i * (caseW + caseMargin), 0.7 * (caseH + caseMargin));
    }
}

/*ecrit les nombres au dessus du tableau et sur le coté*/
function drawSuperman(iG, jG, sideG, topG) {
    ctx.font = fontHeight + 'px serif';
    for (var i = 0; i < sideG.length; i++) {
        for (var j = 0; j < iG; j++) {
            if (sideG[i][j] == undefined) {
                break;
            } else {
                ctx.fillStyle = 'black';
                ctx.fillText(sideG[i][j], (2 - j) * (caseWidth + caseMargin) + 10, i * (caseHeight + caseMargin) + (jG + 0.65) * (caseHeight + caseMargin))


            }
        }
    }
    for (var i = 0; i < topG.length; i++) {
        for (var j = 0; j < jG; j++) {
            if (topG[i][j] == undefined) {
                break;
            } else {
                ctx.fillText(topG[i][j], (iG + 0.2) * (caseWidth + caseMargin) + i * (caseWidth + caseMargin), (2 - j) * (caseHeight + caseMargin) + 1.8 * (caseHeight + caseMargin))
            }
        }

    }
}


function drawOctopus(iG, jG, sideG, topG) {
    ctx.font = fontHeight + 'px serif';
    for (var i = 0; i < sideG.length; i++) {
        for (var j = 0; j < iG; j++) {
            if (sideG[i][j] == undefined) {
                break;
            } else {
                ctx.fillStyle = 'black';
                ctx.fillText(sideG[i][j], (6-j) * (caseWidth + caseMargin) + 10, i * (caseHeight + caseMargin) + (jG + 0.65) * (caseHeight + caseMargin))


            }
        }
    }
    for (var i = 0; i < topG.length; i++) {
        for (var j = 0; j < jG; j++) {
            if (topG[i][j] == undefined) {
                break;
            } else {
                ctx.fillText(topG[i][j], (iG + 0.2) * (caseWidth + caseMargin) + i * (caseWidth + caseMargin),(6-j)* (caseHeight + caseMargin) + 1.8 * (caseHeight + caseMargin))
            }
        }

    }
}


// choisit la hauteur oula largeur en dimension de case pour obtenir des carrés
function widthcalculate(colN) {
    let caseW = (gameWidth - (colN - 1) * caseMargin) / colN;
    return caseW;
}

function heightcalculate(rowN) {
    let caseH = (gameHeight - (rowN - 1) * caseMargin) / rowN;
    return caseH;
}

// on veut des cases carrés
function squarecase(caseW, caseH) {
    if (caseW < caseH) {
        caseH = caseW;
    } else {
        caseW = caseH;
    };
    return caseW;
}

function fontcalculate(caseH) {
    let fontH = 2 * caseH / 3;
    ctx.fillStyle = 'black';
    return fontH;
}




function titanicDisplay() {
    rowNumber = 6;
    colNumber = 6;
    caseWidth = widthcalculate(colNumber);
    caseHeight = heightcalculate(rowNumber);
    fontHeight = fontcalculate(caseHeight);
    caseWidth = squarecase(caseWidth, caseHeight);
    clearGrille();
    grille(iTitanic, jTitanic, rowNumber, colNumber, caseWidth, caseHeight);
    drawTitanic(fontHeight, caseHeight, caseWidth);

    document.oncontextmenu = new Function("return false"); //permet de supprimer la foction accrochée au clic droit par defaut
    canvas.onclick = mouseClick;
    canvas.oncontextmenu = mouseRightClic;

}

function supermanDisplay() {
    rowNumber = 18;
    colNumber = 13;

    caseWidth = widthcalculate(colNumber);
    caseHeight = heightcalculate(rowNumber);
    fontHeight = fontcalculate(caseHeight);
    caseWidth = squarecase(caseWidth, caseHeight);

    clearGrille();

    grille(iSuperman, jSuperman, rowNumber, colNumber, caseWidth, caseHeight);

    drawSuperman(iSuperman, jSuperman, sideSuperman, topSuperman);

}

function octopusDisplay() {
    rowNumber = 65;
    colNumber = 57

    caseWidth = widthcalculate(colNumber);
    caseHeight = heightcalculate(rowNumber);
    fontHeight = fontcalculate(caseHeight);
    caseWidth = squarecase(caseWidth, caseHeight);

    clearGrille();

    grille(iOctopus, jOctopus, rowNumber, colNumber, caseWidth, caseHeight);

    drawOctopus(iOctopus, jOctopus, sideOctopus, topOctopus);

}

/*fait le clic gauche*/
function showCoords(x, y, caseW, caseH, rowN, colN, ) {
    ctx.fillStyle = 'red';
    if (x == 0 || y == 0) {
        return;
    } else if (x >= colN || y >= rowN) {
        return;
    } else {
        ctx.fillRect(x * (caseW + caseMargin), y * (caseH + caseMargin), caseW, caseH);
        tableauVerif[y][x] = 1;
    }
}

function showRight(x, y, caseW, caseH, rowN, colN) {
    ctx.fillStyle = 'black';
    if (x ==0 || y == 0) {
        return;
    } else if (x >= colN || y >= rowN) {
        return;
    } else {
        ctx.fillRect(x * (caseW + caseMargin), y * (caseH + caseMargin), caseW, caseH);
        tableauVerif[y][x] = 0;
    }
}

canvas.onclick = mouseClick;
canvas.oncontextmenu = mouseRightClic;


const buttonTitanic = document.getElementById('boutonT');
buttonTitanic.addEventListener("click", titanicDisplay, false);

const buttonSuperman = document.getElementById('boutonS');
buttonSuperman.addEventListener("click", supermanDisplay, false);

const buttonOctopus = document.getElementById('boutonO');
buttonOctopus.addEventListener("click", octopusDisplay, false);



function mouseClick(event, ) {
    var coinx = Math.floor(event.offsetX / (caseWidth + caseMargin)); //coinx est donc l'abscisse du carré
    var coiny = Math.floor(event.offsetY / (caseHeight + caseMargin));
    showCoords(coinx, coiny, caseWidth, caseHeight, rowNumber, colNumber);
}

function mouseRightClic(event) {
    var coinx = Math.floor(event.offsetX / (caseWidth + caseMargin));
    var coiny = Math.floor(event.offsetY / (caseHeight + caseMargin));
    showRight(coinx, coiny, caseWidth, caseHeight, rowNumber, colNumber);
}

//compare le tableau verif avec le tableau solution 
function verif() {
    for (let i = 1; i < rowNumber; i++) {
        for (let j = 1; j < colNumber; j++) {
            if (tableauVerif[i][j] !== tableauSolution[i][j]) {
                alert("looser!!!");
                return;
            }

        }
    }
    alert("Winner!!!");
}

//compteur
var startTime = 0
var start = 0
var end = 0
var diff = 0
var timerID = 0
function chrono(){
	end = new Date()
	diff = end - start
	diff = new Date(diff)
	var msec = diff.getMilliseconds()
	var sec = diff.getSeconds()
	var min = diff.getMinutes()
	var hr = diff.getHours()-1
	if (min < 10){
		min = "0" + min
	}
	if (sec < 10){
		sec = "0" + sec
	}
	if(msec < 10){
		msec = "00" +msec
	}
	else if(msec < 100){
		msec = "0" +msec
	}
	document.getElementById("chronotime").innerHTML = hr + ":" + min + ":" + sec + ":" + msec
	timerID = setTimeout("chrono()", 10)
}
function chronoStart(){
	document.chronoForm.startstop.value = "stop!"
	document.chronoForm.startstop.onclick = chronoStop
	document.chronoForm.reset.onclick = chronoReset
	start = new Date()
	chrono()
}
function chronoContinue(){
	document.chronoForm.startstop.value = "stop!"
	document.chronoForm.startstop.onclick = chronoStop
	document.chronoForm.reset.onclick = chronoReset
	start = new Date()-diff
	start = new Date(start)
	chrono()
}
function chronoReset(){
	document.getElementById("chronotime").innerHTML = "0:00:00:000"
	start = new Date()
}
function chronoStopReset(){
	document.getElementById("chronotime").innerHTML = "0:00:00:000"
	document.chronoForm.startstop.onclick = chronoStart
}
function chronoStop(){
	document.chronoForm.startstop.value = "start!"
	document.chronoForm.startstop.onclick = chronoContinue
	document.chronoForm.reset.onclick = chronoStopReset
	clearTimeout(timerID)
}