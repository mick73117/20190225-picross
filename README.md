Projet Picross
==============

# Objectifs
* Découverte des éléments de base de l'algo (variables, conditions, opérateurs, boucles, ...)
* Compétence web dynamique : niveau 1
* Compétence maquettage : niveau 1

# Dispositif pédagogique
* Travail par binôme
* Suivre les étapes une par une
* Contraintes outil :
** bootstrap
** HTML/CSS/JS (sans framework js)

# Etape 1
Initialement on intégrera le puzzle "titanic" (voir ./doc/nonogram) de façon statique dans un tableau js.

Définir les différentes étapes permettant d'arriver à :
* 1. Une page html correspondant à la wireframe dans ./doc/wireframe
* 2. Avoir les fonctionnalités suivantes :
** clic gauche sur une case : case vide
** clic droit sur une case : morceau du puzzle
** validation du puzzle de l'utilisateur

La réflexion doit comprendre :
* L'architecture du projet (dossiers, fichiers)
* Un arbre grossier des fonctions de la partie js
* Qui fait quoi comment
* Un planning de la semaine

Pour finir cette étape vous devez avoir créé un repo git qui est un fork de ce projet avec un fichier suivi_projet.md définissant votre façon de travailler sur les prochains jours.

# Etape 2
Mettre en place le projet défini à l'étape précédente avec comme objectif d'implémenter la page avec les fonctionnalités décrites.

